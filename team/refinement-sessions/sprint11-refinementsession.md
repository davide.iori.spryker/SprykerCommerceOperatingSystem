# SPRINT11 REFINEMENT SESSION

- **Date**: 2022-07-02
- **Facilitator**: James Bond, SM of the Cloud Platform Team

| PARTICIPANTS | ROLE |
| ------ | ------ |
| Cloud Platform Team | _The Team_ |

## TODAY'S GOAL 
Backlog grooming in order to plan and prioritize the new requested feature that adds monitoring capabilities to CD pipelines

## MINUTES OF MEETING

- The PO shows to the team the [feature requirement document](https://gitlab.com/davide.iori.spryker/SprykerCommerceOperatingSystem/-/blob/main/product/feature-monitorCDpipelines.md) emphasizing the importance of the feature for our stakeholders.
- Based upon the inputs and priorities provided by the PO the team agreed on creating the following product backlog items:

| USER STORY ID | TITLE | PRIORITY | DEPENDENCY |
| ------ | ------ | ------ | ------ |
| **US1** | As a Cloud engineer I need to identify usage metrics so that I can monitor health of pipelines. | TOP | NONE|
| **US2** | As a Cloud Architect I need to scout the best (open) monitoring solution so that developer can use it to build the feature | TOP | NONE|
| **US3.1** | As a Cloud engineer I need to expose metrics so that data can be collected and sent to the notification system - MVP| TOP | US1, US2|
| **US3.2** | As a Cloud engineer I need to expose metrics so that data can be collected and sent to the notification system| HIGH | US3.1 |
| **US4** | As a Cloud engineer I need build a notification systems so that metrics can be collected and alarms sent to Operations| HIGH | US1|
| **US5**| As a Cloud engineer I need to build custom dashboards so that the Operations Team can use them to visualize the health of pipelines| MEDIUM | US3, US4| 

- **The PO decided to change scope of the Sprint and add cards related to top priorities into the Sprint. The whole Team committed to the new scope and decided to work on these cards. Some, less prio cards, have been taken out from the Sprint Backlog.**

- **The PO and the team agreed on the following delivery plan of the new feature**:

```mermaid
gantt
    title delivery plan new feature
    dateFormat  YYYY-MM-DD
    excludes    weekends
    section Sprint 11
    metrics definition (US1)          :a1, 2022-07-01, 5d
    design solution (US2)     : a2, 2022-07-01, 5d
    build MVP monitoring capabilities (US3.1)       : a3, after a2, 5d
    MVP Functionality v2.0.1                 :milestone, 2022-07-15, 0d
    section Sprint 12
    Build notification systems (US4)     : a4, after a3, 5d
    Build monitoring capabilities (US3.2)          :a5, after a3, 10d
    Functionality v2.1.0                 :milestone, 2022-07-29, 0d
    section Sprint 13
    Build Dashboards (US5)     : after a5, 5d
    Enhanced Functionality v2.1.1                 :milestone, 2022-08-12, 0d
```

## OUTCOME

| OWNER | ACTION |
| ------ | ------ |
| Cloud Platform Team | working on newly created user stories, starting from the top priorities one |
| PO | communicate to stakeholders the delivery planning of the feature accordingly to what agreed with the team |
